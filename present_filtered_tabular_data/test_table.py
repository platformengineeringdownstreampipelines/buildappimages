from bs4 import BeautifulSoup
from present_filtered_tabular_data import present_filtered_tabular_data

# pip install pytest beautifulsoup4
# run pytest test_present_filtered_tabular_data.py


def test_present_filtered_tabular_data():
    # Call the function and get its output
    html_output = present_filtered_tabular_data()

    # Parse the output with BeautifulSoup
    soup = BeautifulSoup(html_output, 'html.parser')

    # Find the table in the output
    table = soup.find('table')

    # Check that the table exists
    assert table is not None

    # Check that the table has the right number of rows
    quantityOfTRs = 13
    # Check that the table has rows
    assert len(table.find_all('tr')) == quantityOfTRs

    # Add more assertions based on your specific requirements
    