from flask import Flask, render_template
from flask import request
import json
import pandas as pd
import os
import logging
from azure.storage.blob import BlobServiceClient
from azure.core.exceptions import AzureError, ResourceNotFoundError, HttpResponseError

web_app = Flask(__name__)


@web_app.route('/', methods=['GET', 'POST'])
def present_filtered_tabular_data():
    """
    Retrieves book data from Azure Blob Storage, filters the data based on price,
    and presents the filtered data in a tabular format.

    Returns:
        A rendered HTML page containing a table with the filtered book data.
    """
    connection_string_st = os.getenv('CONNECTION_STRING_ST')
    container_name = os.getenv('CONTAINER_NAME')
    blob_name = os.getenv('BLOB_NAME')
    filtered_books_data_optimized = []

    logging.basicConfig(level=logging.DEBUG)

    # Initialize Azure Storage Blob Service
    try:
        # Create a BlobServiceClient object
        blob_service_client = BlobServiceClient.from_connection_string(connection_string_st)
        # Get a ContainerClient object
        container_client = blob_service_client.get_container_client(container_name)
        # Convert bytes of JSON file to string
        raw_json_string = container_client.download_blob(name=blob_name, blob=blob_name).readall().decode('utf-8')
        # Deserialize JSON data from blob content
        books_data_dict = json.loads(raw_json_string)
        # ?.get('items', []) # Get the list of books deserialized from the JSON response
        books_data_list = books_data_dict.get('items', [])
        # Set the minimum price for the books
        minimum_price4book = 9.99
        # Get the 'filter' query string argument, by default filter is set to true
        is_filter_active = request.args.get('filter', 'true').lower() == 'false'
        # If 'is_filter_active' is true, filter the data
        if not is_filter_active:
            # Filter the list of books by price
            filtered_books_data_optimized = [book for book in books_data_list if 'saleInfo' in book and 'listPrice' in
                                             book['saleInfo'] and 'amount' in book['saleInfo']['listPrice'] and
                                             book['saleInfo']['listPrice']['amount'] > minimum_price4book]
        # Otherwise, show the data as is
        else:
            filtered_books_data_optimized = books_data_list
    except (AzureError, ResourceNotFoundError, HttpResponseError) as azure_error:
        logging.debug(f'An error occurred with Azure: {azure_error}')
    except json.JSONDecodeError as json_error:
        logging.debug(f'An error occurred while decoding JSON: {json_error}')
    except UnicodeDecodeError as unicode_error:
        logging.debug(f'An error occurred while decoding the blob content: {unicode_error}')
    except KeyError as key_error:
        logging.debug(f'A KeyError occurred while filtering data: {key_error}')

    books_df = pd.DataFrame()

    # Loop through each book in the list of serialized from JSON to create a DataFrame of instances of the book class
    for book_dict in filtered_books_data_optimized: # filtered books with price > 11.99
        volume_info = book_dict.get('volumeInfo', {})
        authors = volume_info.get('authors', [])
        # Create a dictionary with the book details
        book_dict = {
            'title': volume_info.get('title', ''),
            'author': authors[0] if authors else 'Author',
            'synopsis': volume_info.get('description', ''),
            'publisher': volume_info.get('publisher', ''),
            'publish_date': volume_info.get('publishedDate', ''),
            'page_count': volume_info.get('pageCount', ''),
            'tags': volume_info.get('categories', []),
        }
        # Append the book dictionary to the df DataFrame
        books_df = pd.concat([books_df, pd.DataFrame([book_dict])], ignore_index=True)

    return render_template('index.html', table=books_df.to_html(), columns=books_df.columns)


if __name__ == '__main__':
    web_app.run(debug=True, host='0.0.0.0')
    # TODO: patch to WSGI server on prd version #app.run(debug=False, host='???????0.0.0.0')
