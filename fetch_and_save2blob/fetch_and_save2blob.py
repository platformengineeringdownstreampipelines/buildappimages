import requests
import os
from azure.storage.blob import BlobServiceClient
from azure.core.exceptions import ResourceNotFoundError, HttpResponseError, ServiceRequestError, ServiceResponseError
import logging

# Load configuration variables
base_url = os.getenv('BASE_URL')
search_string = os.getenv('SEARCH_STRING')
connection_string_st = os.getenv('CONNECTION_STRING_ST')  # Azure storage connection string
container_name = os.getenv('CONTAINER_NAME')
pagination_params = os.getenv('PAGINATION_PARAMS')
blob_name = os.getenv('BLOB_NAME')

def validate_env_vars():
    required_env_vars = [
        'BASE_URL',
        'SEARCH_STRING',
        'CONNECTION_STRING_ST',
        'CONTAINER_NAME',
        'BLOB_NAME',
        'PAGINATION_PARAMS'
    ]

    for var in required_env_vars:
        if os.getenv(var) is None:
            raise EnvironmentError(f'Environment variable {var} is not set')

def fetch_data(url: str) -> str:
    response = requests.get(url)
    if response.status_code != 200:
        logging.error(f'Failed to fetch data from {url}. Status code: {response.status_code}')
        raise Exception('Failed to fetch data')
    return response.text


def fetch_and_save2blob():
    """
    Fetches book data from the Google Books API and saves it to Azure Blob Storage.
    Handles such exceptions that may occur while uploading blob to Azure Blob Storage or fetching data from the API:
        ResourceNotFoundError: An error occurred with the Azure service while uploading blob.
        HttpResponseError: An error occurred with the Azure service while uploading blob.
        ServiceRequestError: An error occurred with the Azure service while uploading blob.
        ServiceResponseError: An error occurred with the Azure service while uploading blob.
        ConnectionError: An error occurred while uploading blob or fetching data.
        Exception: An unknown error occurred while uploading blob.
    """
    try:
        # Check if all required environment variables are set
        validate_env_vars()

        # Fetch data
        free_data_provider_url = f'{base_url}{search_string}{pagination_params.format(startIndex=0)}'
        data = fetch_data(free_data_provider_url)

        # Upload data to Azure Blob Storage
        blob_service_client = BlobServiceClient.from_connection_string(connection_string_st)
        blob_client = blob_service_client.get_blob_client(container_name, blob_name)
        blob_client.upload_blob(data, overwrite=True)
    except ResourceNotFoundError as ex:
        logging.error(f'ResourceNotFoundError, blob {blob_name} does not exist, Exception: %s', ex)
        raise
    except (HttpResponseError, ServiceRequestError, ServiceResponseError) as ex:
        logging.error(f'ResourceExistsError, An error occurred with the Azure service while uploading blob {blob_name}, Exception: %s', ex)
        raise
    except ConnectionError as ex:
        logging.error('Connection error: %s', ex)
        raise
    except Exception as ex:
        logging.error('Unknown Exception: %s', ex)
        raise

if __name__ == '__main__':
    fetch_and_save2blob()
    